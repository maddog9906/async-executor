# async-executor

## What is async-executor
`async-executor` provides a convenient factory for Spring task executors that adds Metrics instrumentation to the thread pool and queue.

## Declaring the dependency
```Groovy
    repositories {
        ...
        maven { url 'https://raw.githubusercontent.com/alvin9906/maven-repo/master' }  // maven-repo at github
        ...
    }    
     
   dependencies {
        .....
        compile 'com.battletech.maddog:async-executor:0.3.1-spring-boot-2.1.6' 
        .....
   }
```

## Usage
Using Micrometer
```Groovy
def meterRegistry = new SimpleMeterRegistry()
def executor = new AsyncExecutorMetrics(new AsyncExecutorConfig(
    poolSize: 5,
    queueCapacity: 10,
    name: 'my-task-executor'
))
executor.bindTo(meterRegistry)
```
Using Dropwizard
```Groovy
def metricRegistry = new MetricRegistry()
def executor = new AsyncExecutorMetrics(new AsyncExecutorConfig(
    poolSize: 5,
    queueCapacity: 10,
    name: 'my-task-executor'
))
executor.bindToLegacy(metricRegistry)
```



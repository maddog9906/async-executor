package com.battletech.maddog.asyncexecutor

import com.codahale.metrics.MetricRegistry
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import spock.lang.Specification

class AsyncExecutorMetricsSpec extends Specification {

    MeterRegistry metricRegistry
    MetricRegistry metricRegistryLegacy

    def setup() {
        metricRegistry = new SimpleMeterRegistry()
        metricRegistryLegacy = new MetricRegistry()
    }

    def 'creates pool and queue gauge metrics'() {
        given:
        def executor = new AsyncExecutorMetrics(new AsyncExecutorConfig(
                poolSize: 5,
                queueCapacity: 10,
                name: 'my-task-executor'
        ))
        executor.bindTo(metricRegistry)

        expect:
        metricRegistry.get('task-executor.max-threads').tag("pool-name", "my-task-executor").gauge().value() == Integer.MAX_VALUE
        metricRegistry.get('task-executor.available-threads').tag("pool-name", "my-task-executor").gauge().value() == 0
        metricRegistry.get('task-executor.max-queue-size').tag("pool-name", "my-task-executor").gauge().value() == 10

        when:
        100.times { i ->
            executor.execute({
                Thread.sleep 100
                println "${i} done in thread ${Thread.currentThread().name}"
            })
        }

        then:
        assert metricRegistry.get('task-executor.max-threads').tag("pool-name", "my-task-executor").gauge()
        assert metricRegistry.get('task-executor.available-threads').tag("pool-name", "my-task-executor").gauge()
        assert metricRegistry.get('task-executor.active-threads').tag("pool-name", "my-task-executor").gauge()
        assert metricRegistry.get('task-executor.thread-utilisation').tag("pool-name", "my-task-executor").gauge()

        assert metricRegistry.get('task-executor.max-queue-size').tag("pool-name", "my-task-executor").gauge()
        assert metricRegistry.get('task-executor.queue-size').tag("pool-name", "my-task-executor").gauge()
        assert metricRegistry.get('task-executor.queue-utilisation').tag("pool-name", "my-task-executor").gauge()

        and:
        assert metricRegistry.get('task-executor.max-threads').tag("pool-name", "my-task-executor").gauge().value() == Integer.MAX_VALUE
        assert metricRegistry.get('task-executor.available-threads').tag("pool-name", "my-task-executor").gauge().value() > 5
        assert metricRegistry.get('task-executor.active-threads').tag("pool-name", "my-task-executor").gauge().value() > 0
        assert metricRegistry.get('task-executor.thread-utilisation').tag("pool-name", "my-task-executor").gauge().value() > 0.0

        assert metricRegistry.get('task-executor.max-queue-size').tag("pool-name", "my-task-executor").gauge().value() == 10
        assert metricRegistry.get('task-executor.queue-size').tag("pool-name", "my-task-executor").gauge().value() == 10
        assert metricRegistry.get('task-executor.queue-utilisation').tag("pool-name", "my-task-executor").gauge().value() == 1.0 as Double

        when:
        Thread.sleep 1000

        then:
        assert metricRegistry.get('task-executor.active-threads').tag("pool-name", "my-task-executor").gauge().value() == 0
        assert metricRegistry.get('task-executor.thread-utilisation').tag("pool-name", "my-task-executor").gauge().value() == 0.0 as Double

        assert metricRegistry.get('task-executor.queue-size').tag("pool-name", "my-task-executor").gauge().value() == 0
        assert metricRegistry.get('task-executor.queue-utilisation').tag("pool-name", "my-task-executor").gauge().value() == 0.0 as Double

    }

    def 'creates pool and queue gauge metrics legacy'() {
        given:
        def executor = new AsyncExecutorMetrics(new AsyncExecutorConfig(
                poolSize: 5,
                queueCapacity: 10,
                name: 'my-task-executor'
        ))
        executor.bindToLegacy(metricRegistryLegacy)

        expect:
        metricRegistryLegacy.gauges['task-executor.my-task-executor.max-threads'].value == Integer.MAX_VALUE
        metricRegistryLegacy.gauges['task-executor.my-task-executor.available-threads'].value == 0
        metricRegistryLegacy.gauges['task-executor.my-task-executor.max-queue-size'].value == 10

        when:
        100.times { i ->
            executor.execute({
                Thread.sleep 100
                println "${i} done in thread ${Thread.currentThread().name}"
            })
        }

        then:
        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.max-threads'
        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.available-threads'
        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.active-threads'
        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.thread-utilisation'

        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.max-queue-size'
        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.queue-size'
        metricRegistryLegacy.names.contains 'task-executor.my-task-executor.queue-utilisation'

        and:
        metricRegistryLegacy.gauges['task-executor.my-task-executor.max-threads'].value == Integer.MAX_VALUE
        metricRegistryLegacy.gauges['task-executor.my-task-executor.available-threads'].value > 5
        metricRegistryLegacy.gauges['task-executor.my-task-executor.active-threads'].value > 0
        metricRegistryLegacy.gauges['task-executor.my-task-executor.thread-utilisation'].value > 0.0

        metricRegistryLegacy.gauges['task-executor.my-task-executor.max-queue-size'].value == 10
        metricRegistryLegacy.gauges['task-executor.my-task-executor.queue-size'].value == 10
        metricRegistryLegacy.gauges['task-executor.my-task-executor.queue-utilisation'].value == 1.0

        when:
        Thread.sleep 1000

        then:
        metricRegistryLegacy.gauges['task-executor.my-task-executor.active-threads'].value == 0
        metricRegistryLegacy.gauges['task-executor.my-task-executor.thread-utilisation'].value == 0.0

        metricRegistryLegacy.gauges['task-executor.my-task-executor.queue-size'].value == 0
        metricRegistryLegacy.gauges['task-executor.my-task-executor.queue-utilisation'].value == 0.0

    }
}

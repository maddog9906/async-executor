package com.battletech.maddog.asyncexecutor

import com.codahale.metrics.MetricRegistry
import groovy.transform.CompileStatic
import io.micrometer.core.instrument.Gauge
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.binder.BaseUnits

@CompileStatic
class AsyncExecutorMetricsUtils {

    private static final String METRIC_PREFIX = "task-executor"

    static void monitor(MeterRegistry registry, AsyncExecutorMetrics theExecutor, AsyncExecutorConfig config) {
        if (theExecutor && config) {
            Gauge.builder("${METRIC_PREFIX}.max-threads", {theExecutor.maxPoolSize})
                    .tag("pool-name", config.name)
                    .baseUnit(BaseUnits.THREADS)
                    .register(registry)

            Gauge.builder("${METRIC_PREFIX}.available-threads", {theExecutor.poolSize})
                    .tag("pool-name", config.name)
                    .baseUnit(BaseUnits.THREADS)
                    .register(registry)

            Gauge.builder("${METRIC_PREFIX}.active-threads", {theExecutor.activeCount})
                    .tag("pool-name", config.name)
                    .baseUnit(BaseUnits.THREADS)
                    .register(registry)

            Gauge.builder("${METRIC_PREFIX}.thread-utilisation", {(double) (theExecutor.activeCount / theExecutor.maxPoolSize)})
                    .tag("pool-name", config.name)
                    .baseUnit("ratio")
                    .register(registry)

            Gauge.builder("${METRIC_PREFIX}.max-queue-size", {config.queueCapacity})
                    .tag("pool-name", config.name)
                    .baseUnit(BaseUnits.THREADS)
                    .register(registry)

            Gauge.builder("${METRIC_PREFIX}.queue-size", {config.queueCapacity - theExecutor.queueRemainingCapacity})
                    .tag("pool-name", config.name)
                    .baseUnit(BaseUnits.THREADS)
                    .register(registry)

            Gauge.builder("${METRIC_PREFIX}.queue-utilisation", {
                int queueSize = config.queueCapacity - theExecutor.queueRemainingCapacity
                (double) (queueSize / config.queueCapacity)
            })
                    .tag("pool-name", config.name)
                    .baseUnit("ratio")
                    .register(registry)
        }
    }

    static void monitorLegacy(MetricRegistry metricRegistry, AsyncExecutorMetrics theExecutor, AsyncExecutorConfig config) {
        if (theExecutor && config) {
            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'max-threads'), (com.codahale.metrics.Gauge<Integer>) {
                theExecutor.maxPoolSize
            })

            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'available-threads'), (com.codahale.metrics.Gauge<Integer>) {
                theExecutor.poolSize
            })

            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'active-threads'), (com.codahale.metrics.Gauge<Integer>) {
                theExecutor.activeCount
            })

            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'thread-utilisation'), (com.codahale.metrics.Gauge<Double>) {
                (double) (theExecutor.activeCount / theExecutor.maxPoolSize)
            })

            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'max-queue-size'), (com.codahale.metrics.Gauge<Integer>) {
                config.queueCapacity
            })

            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'queue-size'), (com.codahale.metrics.Gauge<Integer>) {
                config.queueCapacity - theExecutor.queueRemainingCapacity
            })

            metricRegistry.register(MetricRegistry.name(METRIC_PREFIX, config.name, 'queue-utilisation'), (com.codahale.metrics.Gauge<Double>) {
                int queueSize = config.queueCapacity - theExecutor.queueRemainingCapacity
                (double) (queueSize / config.queueCapacity)
            })
        }
    }
}

package com.battletech.maddog.asyncexecutor

import com.codahale.metrics.MetricRegistry
import groovy.transform.CompileStatic
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.binder.MeterBinder

import java.util.concurrent.BlockingQueue

@CompileStatic
class AsyncExecutorMetrics extends ThreadPoolTaskExecutorMDC implements MeterBinder {
    private final AsyncExecutorConfig config

    protected BlockingQueue<Runnable> queue

    AsyncExecutorMetrics(AsyncExecutorConfig config) {
        this.config = new AsyncExecutorConfig(
                name: config.name,
                poolSize: config.poolSize?:1,
                maxPoolSize: config.maxPoolSize?:Integer.MAX_VALUE,
                queueCapacity: config.queueCapacity?:Integer.MAX_VALUE,
                mdcPropagationEnabled: config.mdcPropagationEnabled
        )
        createAndInitExecutor()
    }

    @Override
    void bindTo(MeterRegistry registry) {
        AsyncExecutorMetricsUtils.monitor(registry, this, this.config)
    }

    void bindToLegacy(MetricRegistry metricRegistry) {
        AsyncExecutorMetricsUtils.monitorLegacy(metricRegistry, this, this.config)
    }

    @Override
    protected BlockingQueue<Runnable> createQueue(int queueCapacity) {
        queue = super.createQueue(queueCapacity)
        return queue
    }

    int getQueueRemainingCapacity() {
        queue.remainingCapacity()
    }

    private void createAndInitExecutor() {
        this.mdcPropagationEnable =  this.config.mdcPropagationEnabled
        this.corePoolSize = this.config.poolSize
        this.maxPoolSize = this.config.maxPoolSize
        this.queueCapacity = this.config.queueCapacity
        this.threadNamePrefix = "${this.config.name}-"
        this.initialize()
    }
}

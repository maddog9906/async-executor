package com.battletech.maddog.asyncexecutor

import groovy.transform.CompileStatic
import org.slf4j.MDC
import java.util.concurrent.Callable

@CompileStatic
class CallableWrapperWithMDC<T> implements Callable<T> {
    private final Callable<T> wrapped
    private final Map<String, String> map

    CallableWrapperWithMDC(Callable<T> wrapped) {
        this.wrapped = wrapped
        // we are in the origin thread: capture the MDC
        map = MDC.getCopyOfContextMap()
    }

    @Override
    T call() throws Exception {
        // we are in the execution thread: set the original MDC
        T ret = null
        try{
            if (map) {
                MDC.setContextMap(map)
            }
            ret = wrapped.call()
        } finally {
            MDC.clear()
        }
        ret
    }
}

@CompileStatic
class RunnableWrapperWithMDC implements Runnable {
    private final Runnable wrapped
    private final Map<String, String> map

    RunnableWrapperWithMDC(Runnable wrapped) {
        this.wrapped = wrapped
        // we are in the origin thread: capture the MDC
        map = MDC.getCopyOfContextMap()
    }

    @Override
    void run() {
        // we are in the execution thread: set the original MDC
        try {
            if (map) {
                MDC.setContextMap(map)
            }
            wrapped.run()
        } finally {
            MDC.clear()
        }
    }
}

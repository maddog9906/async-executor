package com.battletech.maddog.asyncexecutor

import groovy.transform.CompileStatic
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.util.concurrent.ListenableFuture

import java.util.concurrent.Callable
import java.util.concurrent.Future

@CompileStatic
class ThreadPoolTaskExecutorMDC extends ThreadPoolTaskExecutor {

    boolean mdcPropagationEnable = false

    @Override
    void execute(Runnable task) {
        super.execute(mdcPropagationEnable?new RunnableWrapperWithMDC(task): task)
    }

    @Override
    void execute(Runnable task, long startTimeout) {
        execute(task)
    }

    @Override
    <T> Future<T> submit(Callable<T> task) {
        super.submit(mdcPropagationEnable? new CallableWrapperWithMDC<>(task) : task)
    }

    @Override
    Future<?> submit(Runnable task) {
        super.submit(mdcPropagationEnable? new RunnableWrapperWithMDC(task) : task)
    }

    @Override
    ListenableFuture<?> submitListenable(Runnable task) {
        super.submitListenable(mdcPropagationEnable? new RunnableWrapperWithMDC(task) : task)
    }

    @Override
    <T> ListenableFuture<T> submitListenable(Callable<T> task) {
        super.submitListenable(mdcPropagationEnable? new CallableWrapperWithMDC<>(task) : task)
    }

}

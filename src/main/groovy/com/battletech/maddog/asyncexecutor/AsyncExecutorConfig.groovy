package com.battletech.maddog.asyncexecutor

import groovy.transform.CompileStatic
import groovy.transform.ToString
import groovy.transform.builder.Builder

@CompileStatic
@Builder
@ToString
class AsyncExecutorConfig {
    String name
    int poolSize
    int maxPoolSize
    int queueCapacity
    boolean mdcPropagationEnabled
}